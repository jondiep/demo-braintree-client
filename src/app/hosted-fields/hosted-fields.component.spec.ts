import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HostedFieldsComponent } from './hosted-fields.component';

describe('HostedFieldsComponent', () => {
  let component: HostedFieldsComponent;
  let fixture: ComponentFixture<HostedFieldsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HostedFieldsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HostedFieldsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

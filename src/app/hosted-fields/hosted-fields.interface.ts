export interface ClientTokenRes {
  clientToken: string;
}

// Call made to Braintree for transaction sale
export interface TransactionRes {
  transaction: Transaction;
  success: boolean;
}

export interface Transaction {
  id: string;
  status: string;
  type: string;
  currencyIsoCode: string;
  amount: string;
  merchantAccountId: string;
  subMerchantAccountId: any;
  masterMerchantAccountId: any;
  orderId: any;
  createdAt: string;
  updatedAt: string;
  customer: Customer;
  billing: Billing;
  refundId: any;
  refundIds: any[];
  refundedTransactionId: any;
  partialSettlementTransactionIds: any[];
  authorizedTransactionId: any;
  settlementBatchId: any;
  shipping: Shipping;
  customFields: string;
  avsErrorResponseCode: any;
  avsPostalCodeResponseCode: string;
  avsStreetAddressResponseCode: string;
  cvvResponseCode: string;
  gatewayRejectionReason: any;
  processorAuthorizationCode: string;
  processorResponseCode: string;
  processorResponseText: string;
  additionalProcessorResponse: any;
  voiceReferralNumber: any;
  purchaseOrderNumber: any;
  taxAmount: any;
  taxExempt: boolean;
  creditCard: CreditCard;
  statusHistory: StatusHistory[];
  planId: any;
  subscriptionId: any;
  subscription: Subscription;
  addOns: any[];
  discounts: any[];
  descriptor: Descriptor;
  recurring: boolean;
  channel: any;
  serviceFeeAmount: any;
  escrowStatus: any;
  disbursementDetails: DisbursementDetails;
  disputes: any[];
  authorizationAdjustments: any[];
  paymentInstrumentType: string;
  processorSettlementResponseCode: string;
  processorSettlementResponseText: string;
  networkResponseCode: string;
  networkResponseText: string;
  threeDSecureInfo: any;
  shipsFromPostalCode: any;
  shippingAmount: any;
  discountAmount: any;
  networkTransactionId: string;
  processorResponseType: string;
  authorizationExpiresAt: string;
  refundGlobalIds: any[];
  partialSettlementTransactionGlobalIds: any[];
  refundedTransactionGlobalId: any;
  authorizedTransactionGlobalId: any;
  globalId: string;
  graphQLId: string;
  retryIds: any[];
  retriedTransactionId: any;
  retrievalReferenceNumber: string;
  paypalAccount: PaypalAccount;
  paypalHereDetails: PaypalHereDetails;
  localPayment: LocalPayment;
  coinbaseAccount: CoinbaseAccount;
  applePayCard: ApplePayCard;
  androidPayCard: AndroidPayCard;
  visaCheckoutCard: VisaCheckoutCard;
  masterpassCard: MasterpassCard;
  samsungPayCard: SamsungPayCard;
}

export interface Customer {
  id: any;
  firstName: any;
  lastName: any;
  company: any;
  email: any;
  website: any;
  phone: any;
  fax: any;
}

export interface Billing {
  id: any;
  firstName: any;
  lastName: any;
  company: any;
  streetAddress: any;
  extendedAddress: any;
  locality: any;
  region: any;
  postalCode: any;
  countryName: any;
  countryCodeAlpha2: any;
  countryCodeAlpha3: any;
  countryCodeNumeric: any;
}

export interface Shipping {
  id: any;
  firstName: any;
  lastName: any;
  company: any;
  streetAddress: any;
  extendedAddress: any;
  locality: any;
  region: any;
  postalCode: any;
  countryName: any;
  countryCodeAlpha2: any;
  countryCodeAlpha3: any;
  countryCodeNumeric: any;
}

export interface CreditCard {
  token: any;
  bin: string;
  last4: string;
  cardType: string;
  expirationMonth: string;
  expirationYear: string;
  customerLocation: string;
  cardholderName: any;
  imageUrl: string;
  prepaid: string;
  healthcare: string;
  debit: string;
  durbinRegulated: string;
  commercial: string;
  payroll: string;
  issuingBank: string;
  countryOfIssuance: string;
  productId: string;
  globalId: any;
  graphQLId: any;
  accountType: any;
  uniqueNumberIdentifier: any;
  venmoSdk: boolean;
  maskedNumber: string;
  expirationDate: string;
}

export interface StatusHistory {
  timestamp: string;
  status: string;
  amount: string;
  user: string;
  transactionSource: string;
}

export interface Subscription {
  billingPeriodEndDate: any;
  billingPeriodStartDate: any;
}

export interface Descriptor {
  name: any;
  phone: any;
  url: any;
}

export interface DisbursementDetails {
  disbursementDate: any;
  settlementAmount: any;
  settlementCurrencyIsoCode: any;
  settlementCurrencyExchangeRate: any;
  fundsHeld: any;
  success: any;
}

export interface PaypalAccount {}

export interface PaypalHereDetails {}

export interface LocalPayment {}

export interface CoinbaseAccount {}

export interface ApplePayCard {}

export interface AndroidPayCard {}

export interface VisaCheckoutCard {}

export interface MasterpassCard {}

export interface SamsungPayCard {}

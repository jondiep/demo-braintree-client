import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';

import {
  ClientTokenRes,
  TransactionRes
} from './hosted-fields/hosted-fields.interface';
import {
  Customer,
  CreateCustomerRes,
  SubscriptionRes,
  PlanRes,
  TaxRes
} from './customer/customer.interface';

@Injectable({
  providedIn: 'root'
})
export class AppService {
  constructor(private http: HttpClient) {}

  getClientToken(): Observable<ClientTokenRes> {
    return this.http.get<ClientTokenRes>('http://localhost:8080/client_token');
  }

  requestPaymentMethod(nonce: string): Observable<TransactionRes> {
    return this.http.post<TransactionRes>('http://localhost:8080/checkout', {
      paymentMethodNonce: nonce
    });
  }

  // TODO: Move into customer.service
  createCustomer(
    firstName: string,
    lastName: string,
    email: string,
    paymentMethodNonce: string
  ): Observable<CreateCustomerRes> {
    return this.http.post<CreateCustomerRes>('http://localhost:8080/customer', {
      firstName,
      lastName,
      email,
      paymentMethodNonce
    });
  }

  // TODO: Move into customer.servic
  customerInfo(id: string): Observable<Customer> {
    const params = new HttpParams().set('id', id);
    return this.http.get<Customer>('http://localhost:8080/customer', {
      params
    });
  }

  getSubscriptionPlans(): Observable<PlanRes> {
    return this.http.get<PlanRes>('http://localhost:8080/subscription/plans');
  }

  getTax(postalCode: string): Observable<TaxRes> {
    const params = new HttpParams().set('postalCode', postalCode);
    return this.http.get<TaxRes>('http://localhost:8080/subscription/get-tax', {
      params
    });
  }

  createSubscription(
    paymentMethodToken: string,
    postalCode: string,
    planId: string
  ): Observable<SubscriptionRes> {
    return this.http.post<SubscriptionRes>(
      'http://localhost:8080/subscription/create',
      {
        paymentMethodToken,
        postalCode,
        planId
      }
    );
  }

  modifySubscription(
    subscriptionId: string,
    planId: string,
    paymentMethodToken: string
  ): Observable<any> {
    const body = {
      subscriptionId,
      planId,
      paymentMethodToken
    };

    return this.http.patch('http://localhost:8080/subscription/modify', body);
  }

  cancelSubscription(subscriptionId: string): Observable<SubscriptionRes> {
    return this.http.delete<SubscriptionRes>(
      `http://localhost:8080/subscription/cancel/${subscriptionId}`
    );
  }
}

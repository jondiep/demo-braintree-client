import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CreateCustomerComponent } from './customer/create-customer/create-customer.component';
// NOTE: HostedFields is not used in demo
import { HostedFieldsComponent } from './hosted-fields/hosted-fields.component';
import { SignedInCustomerComponent } from './customer/signed-in-customer/signed-in-customer.component';

const routes: Routes = [
  // { path: '', component: HostedFieldsComponent },
  { path: 'create-customer', component: CreateCustomerComponent },
  { path: 'signed-in', component: SignedInCustomerComponent },
  { path: '', redirectTo: 'create-customer', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}

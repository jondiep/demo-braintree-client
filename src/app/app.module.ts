import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { AppComponent } from './app.component';
import { CreateCustomerComponent } from './customer/create-customer/create-customer.component';
import { SignedInCustomerComponent } from './customer/signed-in-customer/signed-in-customer.component';
import { HostedFieldsComponent } from './hosted-fields/hosted-fields.component';

import { AppRoutingModule } from './app-routing.module';
import { ModifySubscriptionModalComponent } from './customer/signed-in-customer/modify-subscription-modal/modify-subscription-modal.component';
import { NewSubscriptionModalComponent } from './customer/signed-in-customer/new-subscription-modal/new-subscription-modal.component';

@NgModule({
  declarations: [
    AppComponent,
    CreateCustomerComponent,
    HostedFieldsComponent,
    SignedInCustomerComponent,
    ModifySubscriptionModalComponent,
    NewSubscriptionModalComponent
  ],
  imports: [
    AppRoutingModule,
    BrowserModule,
    FormsModule,
    HttpClientModule,
    NgbModule,
    ReactiveFormsModule,
    RouterModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}

interface Response {
  success: boolean;
}

export interface CreateCustomerRes extends Response {
  customer: Customer;
}

export interface PlanRes extends Response {
  plans: Plan[];
}

export interface SubscriptionRes extends Response {
  subscription: Subscription;
}

export interface TaxRes {
  totalRate: number;
  rates: Rate[];
}

export interface Rate {
  rate: number;
  name: string;
  type: string;
}

export interface Customer {
  id: string;
  merchantId: string;
  firstName: string;
  lastName: string;
  company: any;
  email: any;
  phone: any;
  fax: any;
  website: any;
  createdAt: string;
  updatedAt: string;
  customFields: string;
  globalId: string;
  graphQLId: string;
  creditCards: CreditCard[];
  addresses: Address[];
  paymentMethods: PaymentMethod[];
}

export interface CreditCard {
  billingAddress: BillingAddress;
  bin: string;
  cardType: string;
  cardholderName: any;
  commercial: string;
  countryOfIssuance: string;
  createdAt: string;
  customerId: string;
  customerGlobalId: string;
  customerLocation: string;
  debit: string;
  default: boolean;
  durbinRegulated: string;
  expirationMonth: string;
  expirationYear: string;
  expired: boolean;
  globalId: string;
  graphQLId: string;
  healthcare: string;
  imageUrl: string;
  issuingBank: string;
  last4: string;
  payroll: string;
  prepaid: string;
  productId: string;
  subscriptions: any[];
  token: string;
  uniqueNumberIdentifier: string;
  updatedAt: string;
  venmoSdk: boolean;
  verifications: any[];
  maskedNumber: string;
  expirationDate: string;
}

export interface BillingAddress {
  id: string;
  customerId: string;
  firstName: any;
  lastName: any;
  company: any;
  streetAddress: any;
  extendedAddress: any;
  locality: any;
  region: any;
  postalCode: string;
  countryCodeAlpha2: any;
  countryCodeAlpha3: any;
  countryCodeNumeric: any;
  countryName: any;
  createdAt: string;
  updatedAt: string;
}

export interface Address {
  id: string;
  customerId: string;
  firstName: any;
  lastName: any;
  company: any;
  streetAddress: any;
  extendedAddress: any;
  locality: any;
  region: any;
  postalCode: string;
  countryCodeAlpha2: any;
  countryCodeAlpha3: any;
  countryCodeNumeric: any;
  countryName: any;
  createdAt: string;
  updatedAt: string;
}

export interface PaymentMethod {
  billingAddress: BillingAddress2;
  bin: string;
  cardType: string;
  cardholderName: any;
  commercial: string;
  countryOfIssuance: string;
  createdAt: string;
  customerId: string;
  customerGlobalId: string;
  customerLocation: string;
  debit: string;
  default: boolean;
  durbinRegulated: string;
  expirationMonth: string;
  expirationYear: string;
  expired: boolean;
  globalId: string;
  graphQLId: string;
  healthcare: string;
  imageUrl: string;
  issuingBank: string;
  last4: string;
  payroll: string;
  prepaid: string;
  productId: string;
  subscriptions: Subscription[];
  token: string;
  uniqueNumberIdentifier: string;
  updatedAt: string;
  venmoSdk: boolean;
  verifications: any[];
  maskedNumber: string;
  expirationDate: string;
}

export interface BillingAddress2 {
  id: string;
  customerId: string;
  firstName: any;
  lastName: any;
  company: any;
  streetAddress: any;
  extendedAddress: any;
  locality: any;
  region: any;
  postalCode: string;
  countryCodeAlpha2: any;
  countryCodeAlpha3: any;
  countryCodeNumeric: any;
  countryName: any;
  createdAt: string;
  updatedAt: string;
}

export interface Plan {
  id: string;
  merchantId: string;
  billingDayOfMonth: any;
  billingFrequency: number;
  currencyIsoCode: string;
  description: string;
  name: string;
  numberOfBillingCycles: any;
  price: string;
  trialDuration: number;
  trialDurationUnit: string;
  trialPeriod: boolean;
  createdAt: string;
  updatedAt: string;
  addOns: any[];
  discounts: any[];
}

export interface Subscription {
  addOns: any[];
  balance: string;
  billingDayOfMonth: number;
  billingPeriodEndDate: any;
  billingPeriodStartDate: any;
  createdAt: string;
  updatedAt: string;
  currentBillingCycle: number;
  daysPastDue: any;
  discounts: any[];
  failureCount: number;
  firstBillingDate: string;
  id: string;
  merchantAccountId: string;
  neverExpires: boolean;
  nextBillAmount: string;
  nextBillingPeriodAmount: string;
  nextBillingDate: string;
  numberOfBillingCycles: any;
  paidThroughDate: any;
  paymentMethodToken: string;
  planId: string;
  price: string;
  status: string;
  trialDuration: number;
  trialDurationUnit: string;
  trialPeriod: boolean;
  descriptor: Descriptor;
  description: any;
  transactions: any[];
  statusHistory: StatusHistory[];
}

export interface Descriptor {
  name: any;
  phone: any;
  url: any;
}

export interface StatusHistory {
  timestamp: string;
  status: string;
  user: string;
  subscriptionSource: string;
  balance: string;
  price: string;
  currencyIsoCode: string;
  planId: string;
}

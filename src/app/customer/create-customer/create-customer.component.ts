import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { BraintreeError, HostedFields } from 'braintree-web';
import * as braintree from 'braintree-web';
import * as localforage from 'localforage';

import { AppService } from 'src/app/app.service';
import { CreateCustomerRes, Customer } from '../customer.interface';

@Component({
  selector: 'app-create-customer',
  templateUrl: './create-customer.component.html',
  styleUrls: ['./create-customer.component.scss']
})
export class CreateCustomerComponent implements OnInit {
  @ViewChild('hostedFieldsForm') form: ElementRef;
  @ViewChild('submit') submit: ElementRef;
  customer: Customer;
  hostedFields: HostedFields;
  localForage: LocalForage;

  // Form Fields
  formGroup = new FormGroup({
    firstName: new FormControl(''),
    lastName: new FormControl(''),
    company: new FormControl(''),
    email: new FormControl(''),
    phone: new FormControl(''),
    website: new FormControl('')
  });

  constructor(private appService: AppService, private router: Router) {}

  async ngOnInit(): Promise<void> {
    this.localForage = localforage.createInstance({
      name: 'braintree'
    });

    try {
      const customer = await this.localForage.getItem('customerId');
      if (customer !== null) {
        this.router.navigateByUrl('/signed-in');
      } else {
        this.initiateHostedFields();
      }
    } catch (err) {
      console.error(err);
    }
  }

  async initiateHostedFields(): Promise<void> {
    try {
      const clientToken = await (
        await this.appService.getClientToken().toPromise()
      ).clientToken;

      const client = await braintree.client.create({
        authorization: clientToken
      });

      this.hostedFields = await braintree.hostedFields.create({
        client,
        styles: {
          input: {
            'font-size': '1rem',
            color: '#495057'
          },
          'input.invalid': {
            color: 'red'
          },
          'input.valid': {
            color: 'green'
          }
        },
        fields: {
          number: {
            selector: '#card-number',
            placeholder: '4111 1111 1111 1111'
          },
          cvv: {
            selector: '#cvv',
            placeholder: '123'
          },
          expirationDate: {
            selector: '#expiration-date',
            placeholder: '10/2019'
          },
          postalCode: {
            selector: '#zip',
            placeholder: 'Zip Code'
          }
        }
      });

      // Once the fields are initialized enable the submit button
      this.submit.nativeElement.removeAttribute('disabled');
    } catch (err) {
      console.error(err);
    }
  }

  async onSubmit(event: Event): Promise<void> {
    try {
      event.preventDefault();

      const { nonce } = await this.hostedFields.tokenize();

      this.appService
        .createCustomer(
          this.formGroup.value.firstName,
          this.formGroup.value.lastName,
          this.formGroup.value.email,
          nonce
        )
        .subscribe(async ({ customer }: CreateCustomerRes) => {
          this.customer = customer;
          await this.localForage.setItem('customerId', customer.id);
          this.hostedFields.teardown((teardownErr: BraintreeError) => {
            if (teardownErr) {
              // tslint:disable-next-line:quotemark
              console.error("Can't tear down the hosted fields");
            } else {
              // tslint:disable-next-line:no-console
              console.info('Hosted fields has been torn down');
              this.router.navigateByUrl('/signed-in');
            }
          });
        });
    } catch (err) {
      console.error(err);
    }
  }
}

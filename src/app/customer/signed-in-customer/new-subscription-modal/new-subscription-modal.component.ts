import { Component, Input, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { AppService } from 'src/app/app.service';

import {
  PaymentMethod,
  Plan,
  SubscriptionRes,
  TaxRes,
} from '../../customer.interface';

@Component({
  selector: 'app-new-subscription-modal',
  templateUrl: './new-subscription-modal.component.html',
  styleUrls: ['./new-subscription-modal.component.scss'],
})
export class NewSubscriptionModalComponent implements OnInit {
  @Input() activePaymentMethod: PaymentMethod;
  @Input() plan: Plan;
  tax: number;
  total: number;

  constructor(
    private activeModal: NgbActiveModal,
    private appService: AppService
  ) {}

  ngOnInit(): void {
    this.appService
      .getTax(this.activePaymentMethod.billingAddress.postalCode)
      .subscribe(({ totalRate }: TaxRes) => {
        this.tax = Number(this.plan.price) * totalRate;
        this.total = Number(this.plan.price) + this.tax;
      });
  }

  subscribe(): void {
    this.appService
      .createSubscription(
        this.activePaymentMethod.token,
        this.activePaymentMethod.billingAddress.postalCode,
        this.plan.id
      )
      .subscribe(({ subscription }: SubscriptionRes) =>
        this.activeModal.close(subscription)
      );
  }

  dismiss() {
    this.activeModal.dismiss();
  }
}

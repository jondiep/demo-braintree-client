import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SignedInCustomerComponent } from './signed-in-customer.component';

describe('SignedInCustomerComponent', () => {
  let component: SignedInCustomerComponent;
  let fixture: ComponentFixture<SignedInCustomerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SignedInCustomerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SignedInCustomerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

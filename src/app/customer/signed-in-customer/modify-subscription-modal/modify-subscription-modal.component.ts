import { Component, Input, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import {
  Plan,
  Subscription,
  SubscriptionRes
} from 'src/app/customer/customer.interface';
import { AppService } from 'src/app/app.service';

@Component({
  selector: 'app-modify-subscription-modal',
  templateUrl: './modify-subscription-modal.component.html',
  styleUrls: ['./modify-subscription-modal.component.scss']
})
export class ModifySubscriptionModalComponent implements OnInit {
  @Input() subscription: Subscription;
  @Input() subscriptionPlans: Plan[];
  displayedPlans: Plan[];

  constructor(
    private activeModal: NgbActiveModal,
    private appService: AppService
  ) {}

  ngOnInit(): void {
    console.log(this.subscription);
    console.log(this.subscriptionPlans);
    this.displayedPlans = this.subscriptionPlans.filter(
      (plan: Plan) => plan.id !== this.subscription.planId
    );
    console.log(this.displayedPlans);
  }

  dismiss() {
    this.activeModal.dismiss();
  }

  modify(planId: string): void {
    this.appService
      .modifySubscription(
        this.subscription.id,
        planId,
        this.subscription.paymentMethodToken
      )
      .subscribe((data: SubscriptionRes) => this.activeModal.close(data));
  }
}

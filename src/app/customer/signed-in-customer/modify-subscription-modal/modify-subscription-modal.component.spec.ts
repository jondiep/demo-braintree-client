import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModifySubscriptionModalComponent } from './modify-subscription-modal.component';

describe('ModifySubscriptionModalComponent', () => {
  let component: ModifySubscriptionModalComponent;
  let fixture: ComponentFixture<ModifySubscriptionModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModifySubscriptionModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModifySubscriptionModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

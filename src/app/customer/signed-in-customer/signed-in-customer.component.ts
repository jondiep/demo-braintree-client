import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import * as localforage from 'localforage';
import flatten from 'lodash-es/flatten';

import { ModifySubscriptionModalComponent } from './modify-subscription-modal/modify-subscription-modal.component';
import { NewSubscriptionModalComponent } from './new-subscription-modal/new-subscription-modal.component';
import { AppService } from 'src/app/app.service';
import {
  Customer,
  PaymentMethod,
  Plan,
  PlanRes,
  Subscription, // TODO: How to manage with rxjs Subscription? import { Subscription as RXJSSubscription } ... ?
  SubscriptionRes
} from 'src/app/customer/customer.interface';

@Component({
  selector: 'app-signed-in-customer',
  templateUrl: './signed-in-customer.component.html',
  styleUrls: ['./signed-in-customer.component.scss']
})
export class SignedInCustomerComponent implements OnInit {
  activeSubscriptions: Subscription[] = [];
  activePaymentMethod: PaymentMethod;
  canceledSubscriptions: Subscription[] = [];
  customer: Customer;
  localForage: LocalForage;
  subscriptionPlans: Plan[];

  constructor(
    private appService: AppService,
    private modalService: NgbModal,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.localForage = localforage.createInstance({
      name: 'braintree'
    });

    this.getCustomer();
  }

  async getCustomer(): Promise<void> {
    try {
      const customerId = await this.localForage.getItem<string>('customerId');
      if (customerId === null) {
        await this.localForage.clear();
        this.router.navigateByUrl('/create-customer');
      } else {
        this.appService
          .customerInfo(customerId)
          .subscribe((customer: Customer) => {
            console.log(customer);
            this.customer = customer;
            this.activePaymentMethod = customer.paymentMethods[0];
            const subscriptions = flatten(
              customer.paymentMethods.map(
                (paymentMethod: PaymentMethod) => paymentMethod.subscriptions
              )
            );

            this.activeSubscriptions = subscriptions.filter(
              (sub: Subscription) => sub.status === 'Active'
            );
            this.canceledSubscriptions = subscriptions.filter(
              (sub: Subscription) => sub.status === 'Canceled'
            );

            this.getSubscriptionPlans();
          });
      }
    } catch (err) {
      console.error(err);
    }
  }

  getSubscriptionPlans(): void {
    this.appService
      .getSubscriptionPlans()
      .subscribe(({ plans }: PlanRes) => (this.subscriptionPlans = plans));
  }

  async signUpSubscription(plan: Plan): Promise<void> {
    try {
      const modalRef = this.modalService.open(NewSubscriptionModalComponent);
      modalRef.componentInstance.activePaymentMethod = this.activePaymentMethod;
      modalRef.componentInstance.plan = plan;

      const subscription: Subscription = await modalRef.result;
      console.log(subscription);
      this.activeSubscriptions.push(subscription);
      // this.activeSubscriptions.push(subscription);
    } catch (err) {
      console.error(`closed modal. error: ${err}`);
    }
    // this.appService
    //   .createSubscription(
    //     this.activePaymentMethod.token,
    //     this.activePaymentMethod.billingAddress.postalCode,
    //     subscriptionType
    //   )
    //   .subscribe(({ subscription }: SubscriptionRes) =>
    //     this.activeSubscriptions.push(subscription)
    //   );
  }

  async modifySubscription(subscription: Subscription): Promise<void> {
    try {
      const modalRef = this.modalService.open(ModifySubscriptionModalComponent);
      modalRef.componentInstance.subscription = subscription;
      modalRef.componentInstance.subscriptionPlans = this.subscriptionPlans;

      const data: SubscriptionRes = await modalRef.result;
      const activeSubscriptionIdx = this.activeSubscriptions
        .map((sub: Subscription) => sub.id)
        .indexOf(subscription.id);

      this.activeSubscriptions[activeSubscriptionIdx] = data.subscription;
    } catch (err) {
      console.error(`closed modal. error: ${err}`);
    }
  }

  cancelSubscription(subscriptionId: string) {
    this.appService
      .cancelSubscription(subscriptionId)
      .subscribe((data: SubscriptionRes) => {
        this.activeSubscriptions = this.activeSubscriptions.filter(
          (subscription: Subscription) => subscription.id !== subscriptionId
        );
        this.canceledSubscriptions.push(data.subscription);
      });
  }

  async signOut(): Promise<void> {
    try {
      await this.localForage.removeItem('customerId');
    } catch (err) {
      console.error(err);
    } finally {
      this.router.navigateByUrl('/create-customer');
    }
  }
}
